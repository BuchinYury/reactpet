import {applyMiddleware, compose, createStore} from 'redux';
import rootReducer from './reducers';
import thunkMiddleware from 'redux-thunk';


const storeEnhancer = () => {
    const middlewares = applyMiddleware(thunkMiddleware);
    return !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
        ? compose(
            middlewares,
            window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
        )
        : middlewares
};

export default createStore(
    rootReducer,
    storeEnhancer()
);