import axios from 'axios';


const instance = axios.create({
    baseURL: "https://geocode-maps.yandex.ru/1.x/"
});

export const yandexApi = {
    getAddresses: (geocode) => {
        return instance.get(`?format=json&geocode=${geocode}`)
            .then(response => response.data)
    }
};