import React from 'react';
import Addresses from './Addresses';
import {yandexApi} from "../../dal/yandex-geocode/yandex-geocode-api";


class AddressesContainer extends React.Component {
    state = {
        searchString: '',
        addresses: null
    };

    componentDidMount() {
        console.log(yandexApi);
    }

    onSearchStringChange = (searchString) => {
        yandexApi.getAddresses(searchString)
            .then(data => {
                const addresses = data.response.GeoObjectCollection.featureMember.map((addressContainer) => {
                    return addressContainer.GeoObject.metaDataProperty.GeocoderMetaData.text
                });

                this.setState({
                    addresses: addresses
                });
            })
            .catch(() => {
                this.setState({
                    addresses: null
                });
            });

        this.setState({
            searchString: searchString
        });
    };

    render() {
        return (
            <Addresses
                searchString={this.state.searchString}
                addresses={this.state.addresses}
                onSearchStringChange={this.onSearchStringChange}
            />
        )
    }
}

export default AddressesContainer