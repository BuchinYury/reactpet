import React from 'react';
import Address from "./address/Address";

export default function (props) {
    const onSearchStringChange = (event) => {
        props.onSearchStringChange(event.target.value)
    };

    const addresses = props.addresses && props.addresses.map((address, i) => <Address key={i}
                                                                                      address={address}/>);

    return (
        <div>
            <input type="text"
                   name="searchString"
                   autoComplete="off"
                   value={props.searchString}
                   onChange={onSearchStringChange}
            />

            {addresses}
        </div>
    )
}