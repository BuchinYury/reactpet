import React from 'react'
import {Provider} from 'react-redux';
import appStore from '../../bll/store';
import {HashRouter, Route} from 'react-router-dom';
import Login from '../login/Login';
import AddressesContainer from "../addresses/AddressesContainer";


export default function () {
    return (
        <Provider store={appStore}>
            <h1>React/Redux Pet</h1>

            <HashRouter>
                <Route exact
                       path="/"
                       component={Login}
                />
                <Route exact
                       path="/addresses"
                       component={AddressesContainer}
                />
            </HashRouter>
        </Provider>
    )
}