import React from 'react';


export default class Auth extends React.Component {
    onEmailChange = (event) => {
        this.props.setEmailText(event.target.value)
    };
    onPasswordChange = (event) => {
        this.props.setPasswordText(event.target.value)
    };
    onSignInButtonClick = () => {
        this.props.testThunx()
    };

    render() {
        console.log("Auth", this.props);

        return (
            <div className="auth">
                <h3>Sign In</h3>
                <div>
                    <input type="text"
                           name="login"
                           autoComplete="off"
                           placeholder="E-mail"
                           spellCheck={false}
                           value={this.props.email}
                           onChange={this.onEmailChange}
                    />
                </div>
                <div>
                    <input type="password"
                           name="password"
                           placeholder="Password"
                           value={this.props.password}
                           onChange={this.onPasswordChange}
                    />
                </div>
                <button onClick={this.onSignInButtonClick}>Sign In</button>
            </div>
        );
    }
}