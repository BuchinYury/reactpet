import React from 'react';
import Auth from './Auth';
import {connect} from 'react-redux'
import {setEmailText, setPasswordText} from "../../../bll/auth/actions";
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'


class AuthContainer extends React.Component {
    render() {
        console.log("AuthContainer", this.props);

        return (
            <Auth email={this.props.email}
                  password={this.props.password}
                  setEmailText={this.props.setEmailText}
                  setPasswordText={this.props.setPasswordText}
                  testThunx={this.props.testThunx}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setEmailText: bindActionCreators(setEmailText, dispatch),
        setPasswordText: bindActionCreators(setPasswordText, dispatch),
        testThunx: () => {
            const testThunx = () => {
                return dispatch => {
                    setTimeout(
                        () => {
                            console.log('TEST_THUNX');
                            dispatch({type: 'TEST_THUNX'})
                        },
                        1000
                    )
                }
            };

            dispatch(testThunx())
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(AuthContainer))