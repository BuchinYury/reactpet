import React from 'react';
import AuthContainer from './auth/AuthContainer';
import RegistrationContainer from './registration/RegistrationContainer';
import './Login.css'


export default function (props) {
    console.log("Login", props);

    return (
        <div className="Login__root">
            <AuthContainer/>
            <RegistrationContainer/>
        </div>
    )
}